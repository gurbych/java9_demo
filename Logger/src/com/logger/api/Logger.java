package com.logger.api;

public interface Logger extends AutoCloseable {
	public abstract void log(String str);
	public abstract void close();
}
