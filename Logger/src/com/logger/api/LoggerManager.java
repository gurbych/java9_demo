package com.logger.api;

import com.logger.internal.ConsoleLoggerImpl;
import com.logger.internal.FileLoggerImpl;
import com.logger.internal.SocketLoggerImpl;

public interface LoggerManager {
	public static final String CONSOLE = "ConsoleLogger";
	public static final String FILE = "FileLogger";
	public static final String SOCKET = "SocketLogger";
	
	public static Logger getConsoleLogger() {
		return getLogger(CONSOLE);
	}
	public static Logger getFileLogger() {
		return getLogger(FILE);
	}
	public static Logger getSocketLogger() {
		return getLogger(SOCKET);
	}
	
	private static Logger getLogger(String type) {
		switch(type) {
		case CONSOLE:
			return new ConsoleLoggerImpl();
		case FILE:
			return new FileLoggerImpl();
		case SOCKET:
			return new SocketLoggerImpl();
			default:
				throw new RuntimeException(String.format("%s logger type is not implemented!", type));
		}
	}
}
