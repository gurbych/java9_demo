package com.logger.internal;

import java.time.LocalDateTime;

import com.logger.api.Logger;
import com.logger.api.LoggerManager;

public class ConsoleLoggerImpl implements Logger {

	@Override
	public void log(String str) {
		System.out.printf("DateTime: %s; Thread: %s; Logger: %s; Message: \n%s\n", LocalDateTime.now(), Thread.currentThread().getName(), LoggerManager.CONSOLE, str);
	}

	@Override
	public void close() {
		// Does nothing
	}

}
