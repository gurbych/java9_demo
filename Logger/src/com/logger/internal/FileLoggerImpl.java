package com.logger.internal;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import com.logger.api.Logger;
import com.logger.api.LoggerManager;

public class FileLoggerImpl implements Logger {
	private final PrintWriter out;
	
	public FileLoggerImpl() {
		try {
			out = new PrintWriter("log.txt");
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void log(String str) {
		out.printf("DateTime: %s; Thread: %s; Logger: %s; Message: \n%s\n", LocalDateTime.now(), Thread.currentThread().getName(), LoggerManager.FILE, str);
	}

	@Override
	public void close() {
		out.flush();
		out.close();
	}

}
