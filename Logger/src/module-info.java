module com.logger {
	// It is implicit, whether we write it or not
	requires java.base;
	
	 // expose java.base to modules depending on this one
//	requires transitive java.xml;
	
	// Regular export
	exports com.logger.api;
	
	// QUALIFIED EXPORT
	// Only to specific module; invisible for others
	//	exports com.logger.api to com.application;
	
	// Provide only an interface
	provides com.logger.api.Logger  // interface name
		with com.logger.internal.FileLoggerImpl;  // current implementation - we can change it later!
	
	// Closed at compile-time, open at runtime
	// For reflection and frameworks - Hibernate, Spring which use it
	opens com.logger.internal;
}