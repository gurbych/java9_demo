
public class DiamondOperatorMain {

	public static void main(String[] args) {
		Box<String> box = Box.getBox("Something nice");
		System.out.println(box.getContent());
	}
	
	static class Box<T> {
		private T content;
		
		public Box(T t) {
			this.content = t;
		}
		
		T getContent() {
			return content;
		}
		
		// Old
//		public static <T> Box<T> getBox(T content) {
//		    // we have to put the `T` here :(
//		    return new Box<T>(content);
//		}
		
		// New
		public static <T> Box<T> getBox(T content) {
		    // we have to put the `T` here :(
		    return new Box<>(content);
		}
	}

}
