import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class TryWithResourcesMain {
	private static final String userName = "admin";
	private static final String password = "admin";
	private static final String dbms = "derby";
	private static final String serverName = "creamer";
	private static final String dbName = "COFFEEBREAK";
	private static final int portNumber = 9040;

	public static void main(String[] args) throws SQLException {
		Connection conn = getConnection();

	}
	
	// old way
//	public static void close(Connection connection) throws Exception {
//	    try(Connection c = connection) {
//	        c.close();
//	    }
//	}
	
	// new way - no need to create intermediate reference
	public static void close(Connection connection) throws Exception {
//		connection = null;
	    try(connection) { // since connection is effectively final
	    	connection.close();
	    }
	}
	
	public static Connection getConnection() throws SQLException {

	    Connection conn = null;
	    Properties connectionProps = new Properties();
	    connectionProps.put("user", userName);
	    connectionProps.put("password", password);

	    if (dbms.equals("mysql")) {
	        conn = DriverManager.getConnection("jdbc:" + dbms + "://" + serverName + ":" + portNumber + "/", connectionProps);
	    } else if (dbms.equals("derby")) {
	        conn = DriverManager.getConnection("jdbc:" + dbms + ":" + dbName + ";create=true", connectionProps);
	    }
	    System.out.println("Connected to database");
	    return conn;
	}

}
