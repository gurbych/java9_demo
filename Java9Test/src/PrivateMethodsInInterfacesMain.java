import java.util.stream.IntStream;

public class PrivateMethodsInInterfacesMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public interface BeforeJava9 {

	    default boolean isEvenSum(int... numbers) {
	        return sum(numbers) % 2 == 0;
	    }

	    default boolean isOddSum(int... numbers) {
	        return sum(numbers) % 2 == 1;
	    }

	    // aww, I don't want to expose it...
	    default int sum(int[] numbers) {
	        return IntStream.of(numbers).sum();
	    }

	}
	
	public interface AfterJava9 {

	    default boolean isEvenSum(int... numbers) {
	        return sum(numbers) % 2 == 0;
	    }

	    default boolean isOddSum(int... numbers) {
	        return sum(numbers) % 2 == 1;
	    }

	    // now it is hidden!
	    private int sum(int[] numbers) {
	        return IntStream.of(numbers).sum();
	    }

	}

}
