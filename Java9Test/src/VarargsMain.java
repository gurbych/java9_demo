import java.util.Optional;

public class VarargsMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	// private, non-final methods could not be annotated before and Java 9 (without any reason)
	// it was just annoying
	@SafeVarargs
	public static <T> Optional<T> first(T... args) {
	    if (args.length == 0)
	        return Optional.empty();
	    else
	        return Optional.of(args[0]);
	}

}

class UnSafeVarargs {
	  static <T> T[] asArray(T... args) {
	    return args;
	  }

	  static <T> T[] arrayOfTwo(T a, T b) {
	    return asArray(a, b);
	  }

	  public static void main(String[] args) {
	    String[] bar = arrayOfTwo("hi", "mom");
	  }
}
