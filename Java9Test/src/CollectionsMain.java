import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectionsMain {

	public static void main(String[] args) {
		// I love typing a LOT of symbols! I love Java!
		// Create small (unmodifiable) collection:
		
		//=============================SET & LIST=============================
		
		// 1: classical, straightforward technique
		
		// 1) instantiate empty collection
		// 2) add some elements
		// 3) make it unmodifiable
		Set<String> set = new HashSet<>();
		set.add("a");
		set.add("b");
		set.add("c");
		set = Collections.unmodifiableSet(set);
		System.out.println(set+", class="+set.getClass().getCanonicalName());
		
		// 2: "triple wrap" technique
		
		// 1) make list from array of elements
		// 2) place those elements to a new Collection
		// 3) make it unmodifiable
		Set<String> set2 = Collections.unmodifiableSet(new HashSet<>(Arrays.asList("a", "b", "c")));
		System.out.println(set2+", class="+set2.getClass().getCanonicalName());
		
		// 3: "double brace" technique
		
		// 1) anonymous inner class instance initiated with instance-initializer construct
		// 2) make it unmodifiable
		Set<String> set3 = Collections.unmodifiableSet(new HashSet<String>() {{
		    add("a"); add("b"); add("c");
		}});
		System.out.println(set3+", class="+set3.getClass().getCanonicalName());
		
		// 4: usage of a stream collector
		
		// 1) init stream
		// 2) collect some crap. Result collection type is unknown and it is mutable
		// 3) make it unmodifiable
		Set<String> set4 = Collections.unmodifiableSet(Stream.of("a", "b", "c").collect(Collectors.toSet()));
		System.out.println(set4+", class="+set4.getClass().getCanonicalName());
		
		// 5: Java 9 approach
		
		Set<String> set5 = Set.of("a", "b", "c");
		System.out.println(set5+", class="+set5.getClass().getCanonicalName());
		
		//=============================MAP=============================
		
		// empty
		Map<Integer, String> map = Map.of();
		System.out.println(map+", class="+map.getClass().getCanonicalName());
		
		// plain
		Map<Integer, String> map2 = Map.of(1, "a", 2, "b", 3, "c");
		System.out.println(map2+", class="+map2.getClass().getCanonicalName());
		
		// from entries
		Map<Integer, String> map3 = Map.ofEntries(Map.entry(1, "a"), Map.entry(2, "b"), Map.entry(3, "c"));
		System.out.println(map3+", class="+map3.getClass().getCanonicalName());
	}

}
