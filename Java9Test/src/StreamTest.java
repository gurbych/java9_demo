import java.util.stream.Stream;

public class StreamTest {

	public static void main(String[] args) {
		
		// 1. static <T> Stream<T> ofNullable​(T t)
		
		Stream.ofNullable(null).forEach(System.out::println);
		Stream.ofNullable("Hello").forEach(System.out::println);
		
		// Some usage
		
		// Java 8
		final String configurationDirectory =
				   Stream.of("app.config", "app.home", "user.home")
				         .flatMap(key -> {
				              final String property = System.getProperty(key);
				              return property == null ? Stream.empty() : Stream.of(property);
				         })
				         .findFirst()
				         .orElseThrow(IllegalStateException::new);
		
		// Java 9
		final String configurationDirectory2 =
				   Stream.of("app.config", "app.home", "user.home")
				         .flatMap(key -> Stream.ofNullable(System.getProperty(key)))
				         .findFirst()
				         .orElseThrow(IllegalStateException::new);
		
		// 2. default Stream<T> takeWhile​(Predicate<? super T> predicate)
		
		Stream.of("a", "b", "c", "", "e").takeWhile(s -> !s.isEmpty()).forEach(System.out::print);
		System.out.println();
		
		// 3. default Stream<T> dropWhile​(Predicate<? super T> predicate)
		
		Stream.of("a", "b", "c", "", "e").dropWhile(s -> !s.isEmpty()).forEach(System.out::print);
		
		// 4. static <T> Stream<T>	iterate​(T seed, UnaryOperator<T> f)
		//    static <T> Stream<T>	iterate​(T seed, Predicate<? super T> hasNext, UnaryOperator<T> next)
		
		Stream.iterate(1, i -> i+1).forEach(System.out::println);
		Stream.iterate(0, i -> i < 10, i -> i + 1).forEach(System.out::println);
	}

}
