package com.application;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ServiceLoader;

import com.logger.api.Logger;

//import com.logger.api.Logger;
//import com.logger.api.LoggerManager;

public class Application {

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		// Straightforward, from imported module
//		Logger logger = LoggerManager.getConsoleLogger();
		
		// Using reflection, from non-imported package - causes IllegalAccessException
		Logger logger = getFromByteCode();
		
		// Using ServiceLoader
//		Logger logger = getUsingServiceLoader();
				try(logger; br) {
			System.out.println("Enter something");
			while(true) {
				String s = br.readLine();
				logger.log(s);
			}
		}
	}
	
	// Regular reflection class load
	private static Logger getFromByteCode() throws Exception {
		ClassLoader classLoader = ClassLoader.getSystemClassLoader();
		Class<?> _class = classLoader.loadClass("com.logger.internal.ConsoleLoggerImpl");
		return (Logger) _class.getConstructor().newInstance();
	}
	
	// ServiceLoader approach: Works after "uses" was added to module-info.java
	private static Logger getUsingServiceLoader() {
		// Creates a new service loader for the given service type, using the current thread's context class loader.
		ServiceLoader<Logger> loggerService = ServiceLoader.load(Logger.class);
		return loggerService.findFirst().get();
	}

}
