module com.application {
	exports com.application;
	requires com.logger;
	
	// We consume only an interface, we don't know anything about an implementation
//	uses com.logger.api.Logger; // interface name!
}